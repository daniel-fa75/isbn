﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISBN_Pruefzifferberechnung
{
    class Program
    {
        public static void Main(string[] args)
        {
            string[] isbns;

            CreateIsbns(out isbns);
            CheckAllIsbns(isbns);

            Console.ReadKey();
        }

        // Erstellen des ISBN Arrays
        public static void CreateIsbns(out string[] isbns)
        {
            isbns = new string[] { "978-3-12-732320-7", "9783960090724", "978-3864902857", "9783446437234", "978-9-08-998815-0", "978-9-08-998716-7", "3-499-13599-x", "3-446-19313-8", "0-7475-5100-6", "1-57231-422-2", "349913599x", "1-234-56789-0" };
        }

        // Überprüfung aller ISBNs
        public static void CheckAllIsbns(string[] isbns)
        {
            string isbn;
            int lastDigit;
            bool validIsbn;

            foreach (string isbnNum in isbns)
            {
                isbn = isbnNum.Replace("-", "");
                lastDigit = 0;

                if (isbn.Length > 10)
                {
                    IsbnCheckDigit(isbn, ref lastDigit);
                    IsValid(Convert.ToInt32(isbn[isbn.Length - 1].ToString()), lastDigit, out validIsbn);
                }
                else
                {
                    IsbnCheckDigit(ref lastDigit, isbn);
                    IsValid(out validIsbn, lastDigit, isbn);
                }

                Console.WriteLine("Die Isbn {0} ist {1}. Die berechnete Prüfziffer lautet {2}.", isbnNum, validIsbn ? "gültig" : "nicht gültig", lastDigit);
            }
        }
        
        // Prüfzifferberechnung der ISBN-10
        public static void IsbnCheckDigit(ref int lastDigit, string isbn)
        {
            for (int i = 0; i < isbn.Length - 1; i++)
            {
                lastDigit = lastDigit + Convert.ToInt32(isbn.Substring(i, 1)) * (i + 1);
            }

            lastDigit = lastDigit % 11;
        }

        // Prüfzifferberechnung der ISBN-13
        public static void IsbnCheckDigit(string isbn, ref int lastDigit)
        {
            int sum = 0;

            for (int i = 0; i < isbn.Length - 1; i++)
            {
                sum += ((i + 1) % 2 == 0) ? Convert.ToInt32(isbn[i].ToString()) * 3 : Convert.ToInt32(isbn[i].ToString());
            }

            lastDigit = (10 - (sum % 10)) % 10;
        }
        
        // Validierung der ISBN-10
        public static void IsValid(out bool validIsbn, int checkDigit, string isbn)
        {
            int isbnCheckDigit = 0;

            if (isbn.Substring(isbn.Length - 1, 1) == "x")
            {
                isbnCheckDigit = 10;
            }
            else
            {
                isbnCheckDigit = Convert.ToInt32(isbn.Substring(isbn.Length - 1, 1));
            }

            validIsbn = (checkDigit == isbnCheckDigit);
        }

        // Validierung der ISBN-13
        public static void IsValid(int lastDigit, int calculatedLastDigit, out bool validIsbn)
        {
            validIsbn = (lastDigit == calculatedLastDigit);
        }
    }
}
